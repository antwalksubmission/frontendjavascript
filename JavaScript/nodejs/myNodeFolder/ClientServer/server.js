const http=require('http');
const url=require('url');
const fs=require('fs');

const hostname='127.0.0.1'
const port=3000

const server=http.createServer((req,res)=>{
    var pathname=url.parse(req.url).pathname;
    
    console.log('request made by '+pathname+'received');
    fs.readFile(pathname.substring(1),function(err,data){
        if(err)
        {
            console.log(err);
            res.writeHead(404,{'Content-type':'text/html'})
        }
        else{
            res.writeHead(200,{'Content-type':'text/html'})
            res.write(data.toString())
        }
        res.end();
    })

    
}).listen(port);
console.log('server is running at http://${hostname}:${port}/');
