const http=require('http')


var reqOption={
    host:'localhost',
    port:3000,
    path:'/index.html'
}

var callBack=function(res){
    var info='';
    res.on('data',function(data){
        info+=data;
    });
    res.on('end',function(){
        console.log(info);
    });
}

var req=http.request(reqOption,callBack)
req.end();