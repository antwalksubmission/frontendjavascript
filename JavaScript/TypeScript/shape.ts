interface Shape{
    height:number;
    width:number;
}

function surface(obj:Shape)
{
    return obj.height*obj.width;
}

class Square{
    height:number;
    width:number;
    weight:number;

    constructor(public h:number,public wd:number,public wg:number)
    {
        this.height=h;
        this.width=wd;
        this.weight=wg;
    }
}

let temp2={h:10,wd:20,wg:10};

let obj2=new Square(10,20,10);

console.log(surface(obj2));
