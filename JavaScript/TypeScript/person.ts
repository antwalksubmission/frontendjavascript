interface Person
{
    firstName:string;
    lastName:string;
}

function greeting(obj:Person)
{
    return "hello "+obj.firstName+" "+obj.lastName;
}

let temp={firstName:"Arijit",lastName:"Seal"}
console.log(greeting(temp));
