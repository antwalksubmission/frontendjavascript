class Student{
    fullName:string;
    constructor(public firstName:string,public middletName:string,public lastName:string)
    {
        this.fullName=firstName+" "+lastName;
    }
}

let obj1=new Student("John","Unklnown","Doe");

interface Person
{
    firstName:string;
    lastName:string;
}

function greeting1(obj:Person)
{
    return "hello "+obj.firstName+" "+obj.lastName;
}

let temp1={firstName:"Arijit",middleName:"Nothing",lastName:"Seal"}
console.log(greeting1(temp1));