function surface(obj) {
    return obj.height * obj.width;
}
var Square = /** @class */ (function () {
    function Square(h, wd, wg) {
        this.h = h;
        this.wd = wd;
        this.wg = wg;
        this.height = h;
        this.width = wd;
        this.weight = wg;
    }
    return Square;
}());
var temp2 = { h: 10, wd: 20, wg: 10 };
var obj2 = new Square(10, 20, 10);
console.log(surface(obj2));
