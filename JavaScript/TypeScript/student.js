var Student = /** @class */ (function () {
    function Student(firstName, middletName, lastName) {
        this.firstName = firstName;
        this.middletName = middletName;
        this.lastName = lastName;
        this.fullName = firstName + " " + lastName;
    }
    return Student;
}());
var obj1 = new Student("John", "Unklnown", "Doe");
function greeting1(obj) {
    return "hello " + obj.firstName + " " + obj.lastName;
}
var temp1 = { firstName: "Arijit", middleName: "Nothing", lastName: "Seal" };
console.log(greeting1(temp1));
